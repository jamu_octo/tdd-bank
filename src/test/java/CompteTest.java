import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CompteTest {

    DateProvider dateProvider;
    TransferArgentApi transferArgentApi;
    Compte compte;

    @Before
    public void setUp(){
        dateProvider = Mockito.mock(DateProvider.class);
        transferArgentApi = Mockito.mock(TransferArgentApi.class);
        compte = new Compte(dateProvider, transferArgentApi);
    }

    @Test
    public void testDepot(){
        //Given
        float transaction = 500f;

        //When
        compte.depot(transaction);

        //Then
        Assert.assertEquals(500f, compte.consulterSolde(),0.001);
    }

    @Test
    public void testRetrait(){
        //Given
        float transaction = 500f;

        //When
        compte.retrait(transaction);

        //Then
        Assert.assertEquals(-500f, compte.consulterSolde(),0.001);
    }

    @Test
    public void testConsulterSolde(){
        //Given
        float transaction = 500f;

        //When
        compte.retrait(transaction);
        float solde = compte.consulterSolde();

        //Then
        Assert.assertEquals(-500f, solde,0.001);
    }

    @Test
    public void testAfficherUneTransaction(){

        //Given

        Mockito.when(dateProvider.now()).thenReturn("02-12-2020 14:30:45");

        //When
        compte.depot(200f);

        //Then
        Assert.assertEquals("02-12-2020 14:30:45 200.0 200.0", compte.afficherHistoriqueTransaction().get(0));

    }

    @Test
    public void testAfficherPlusieursTransactions(){
        //Given

        //When
        compte.depot(200f);
        compte.depot(200f);
        compte.retrait(100f);

        List<String> expectedListTransactions = new ArrayList<>();
        expectedListTransactions.add("02-12-2020 200.0 200.0");
        expectedListTransactions.add("02-12-2020 200.0 400.0");
        expectedListTransactions.add("02-12-2020 -100.0 300.0");

        //Then
        Assert.assertEquals(expectedListTransactions.size(), compte.afficherHistoriqueTransaction().size());
    }

    @Test
    public void verifierFormatHistorique(){
        //Given
        Mockito.when(dateProvider.now()).thenReturn("02-12-2020 14:30:45");

        //When
        compte.depot(200f);
        compte.retrait(100f);
        compte.depot(500f);

        //Then
        Assert.assertEquals("02-12-2020 14:30:45 200.0 200.0", compte.afficherHistoriqueTransaction().get(0));
        Assert.assertEquals("02-12-2020 14:30:45 -100.0 100.0", compte.afficherHistoriqueTransaction().get(1));
        Assert.assertEquals("02-12-2020 14:30:45 500.0 600.0", compte.afficherHistoriqueTransaction().get(2));
    }
    @Test
    public void testTransfertArgentOK(){

        Mockito.when(transferArgentApi.transfer(anyString(),anyString(),anyFloat())).thenReturn("202");

        String httpCode = compte.transferArgent("aze", "abc", 300f);

        verify(transferArgentApi, times(1)).transfer("aze", "abc", 300f);

        Assert.assertEquals("Transfert OK", httpCode);
    }

    @Test
    public void testTransfertArgentKO(){

    }

}
