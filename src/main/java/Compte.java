import java.util.*;

public class Compte {
    private float balance;
    private List<String> historiqueTransactions;
    private DateProvider dateProvider;
    private TransferArgentApi transferArgentApi;

    public Compte(DateProvider dateProvider, TransferArgentApi transferArgentApi){
        this.dateProvider = dateProvider;
        this.transferArgentApi = transferArgentApi;
        this.balance = 0f;
        historiqueTransactions = new ArrayList<>();
    }

    public void depot(float transaction) {
        this.balance += transaction;
        historiqueTransactions.add(getCurrentDate() + " " + transaction + " " + balance);
    }

    public float consulterSolde() {
        return balance;
    }

    public void retrait(float transaction) {
        this.balance -= transaction;
        historiqueTransactions.add(getCurrentDate() + " " + -transaction + " " + balance);
    }

    private String getCurrentDate(){
        return dateProvider.now();
    }

    public List<String> afficherHistoriqueTransaction() {
         return historiqueTransactions;
    }

    public String transferArgent(String ibanFrom, String ibanDestination, float montant) {
        String response = transferArgentApi.transfer(ibanFrom, ibanDestination, montant);
        if(response == "202") return "Transfert OK";
        else return "Transfert KO";
    }
}
